package sending

type Destination struct {
	To        string `json:"to"`
	MessageID string `json:"message_id"`
}

// Запрос на посылку СМС для сервиса. НЕ завязан на запросы провайдеров
type SMS struct {
	Destinations []Destination `json:"destinations"`
	Message      string        `json:"message"`
	Sender       string        `json:"sender"`
	Id           string        `json:"id"`
	Secure       bool          `json:"secure"`
}

type SMSBulk struct {
	BatchId  string `json:"batch_id"`
	Messages []SMS  `json:"messages"`
}
