package status

import "time"

type Destination struct {
	To            string    `json:"to"`
	DestinationID string    `json:"destination_id"`
	Status        int       `json:"status"`
	DtRegistered  time.Time `json:"dt_registered"`
	//DtSent        time.Time `json:"dt_sent"`
	DtDelivered time.Time `json:"dt_delivered"`
	MessageID   string    `json:"message_id"`
}

type Message struct {
	MessageID string `json:"message_id"`
	BatchID   string `json:"batch_id"`
	Text      string `json:"text"`
	From      string `json:"from"`
}

// Локальная сущность СМС для сервиса. НЕ ЗАВЯЗАНА на реализацию структуры СМС какого либо из провайдеров
type SMS struct {
	Destinations []Destination `json:"destinations"`
	Message
}
