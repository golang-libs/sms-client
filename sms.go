package sms_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/golang-libs/sms-client/request/sending"
	"gitlab.com/golang-libs/sms-client/request/status"
	"io/ioutil"
	"net/http"
)

func SendSMS(url string, messages sending.SMSBulk) error {
	b, err := json.Marshal(&messages)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url+"/v1/send", bytes.NewBuffer(b))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func GetSMSState(url string, id string) (status.SMS, error) {
	sms := status.SMS{}
	req, err := http.NewRequest("GET", fmt.Sprintf(url+"/v1/data?message_id=%s", id), nil)
	if err != nil {
		return sms, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return sms, err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return sms, err
	}

	err = json.Unmarshal(b, &sms)
	if err != nil {
		return sms, err
	}

	return sms, nil
}
